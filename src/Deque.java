import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by volosovskijroman on 26.07.15.
 */
public class Deque<Item> implements Iterable<Item> {
    private int size = 0;
    private Container first = null;
    private Container last = null;

    private class Container {
        private Item item;
        private Container next = null;
        private Container prev = null;

        public Container(Item it) {
            item = it;
        }
    }

    private class DequeIterator implements Iterator<Item> {

        private Container f = first;

        @Override
        public boolean hasNext() {
            return f != null;
        }

        @Override
        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            Item it = f.item;
            f = f.next;

            return it;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    private void checkLast() {
        if (size == 0) {
            throw new NoSuchElementException();
        }
    }

    public void addFirst(Item item) {
        checkNull(item);
        Container n = new Container(item);
        if (!isEmpty()) {
            first.prev = n;
            n.next = first;
            first = n;
        } else {
            first = n;
            last = n;
        }
        size++;
    }

    private void checkNull(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }
    }

    public void addLast(Item item) {
        checkNull(item);
        Container n = new Container(item);
        if (!isEmpty()) {
            last.next = n;
            n.prev = last;
            last = n;
        } else {
            first = n;
            last = n;
        }
        size++;
    }

    public Item removeFirst() {
        checkLast();
        Item it = first.item;
        if (last == first) {
            last = null;
        }
        if (first.next != null) {
            first.next.prev = null;
        }
        first = first.next;
        size--;

        return it;
    }

    public Item removeLast() {
        checkLast();
        Item it = last.item;
        if (last == first) {
            first = null;
        }
        if (last.prev != null) {
            last.prev.next = null;
        }
        last = last.prev;
        size--;

        return it;
    }

    public Iterator<Item> iterator() {
        return new DequeIterator();
    }

    public static void main(String[] args) {
        Deque<Integer> de = new Deque<Integer>();
        de.addLast(1);
        de.removeLast();
        de.addLast(3);
        de.removeFirst();
        de.addLast(5);
        de.addLast(6);
        de.addLast(7);
        de.removeLast();

        for (Integer i : de) {
            StdOut.println(i);
        }
        StdOut.println("------");
        StdOut.println(de.size());
    }

}
