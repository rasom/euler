import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by volosovskijroman on 26.07.15.
 */
public class RandomizedQueue<Item> implements Iterable<Item> {
    private int size = 0;
    private int max = 32;
    private Item[] col;

    public RandomizedQueue() {
        col = (Item[]) new Object[max];
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public void enqueue(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }
        if (isEmpty()) {
            col[0] = item;
        } else {
            int idx = StdRandom.uniform(size + 1);
            Item ot = col[idx];
            col[size] = ot;
            col[idx] = item;
            checkSize();
        }

        size++;
    }

    private void checkSize() {
        Boolean maxChanged = false;
        if (size > max * 0.7) {
            maxChanged = true;
            max *= 2;
        } else if (size < max * 0.2) {
            maxChanged = true;
            max /= 2;
        }

        if (maxChanged) {
            Item[] newCol = (Item[]) new Object[max];
            System.arraycopy(col, 0, newCol, 0, size + 1);
            col = newCol;
        }
    }

    public Item dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }

        Item it = col[size - 1];
        col[size - 1] = null;
        checkSize();
        size--;

        return it;
    }

    private class RandomIterator implements Iterator<Item> {
        private int cnt;
        private Item[] items;

        public RandomIterator() {
            cnt = size - 1;
            items = (Item[]) new Object[size];
            System.arraycopy(col, 0, items, 0, size);
            StdRandom.shuffle(items);
        }

        @Override
        public boolean hasNext() {
            return cnt >= 0;
        }

        @Override
        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            Item it = items[cnt];
            cnt--;
            return it;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    public Item sample() {
        if (size == 0) {
            throw new NoSuchElementException();
        }

        return col[StdRandom.uniform(size)];
    }

    public Iterator<Item> iterator() {
        return new RandomIterator();
    }

    public static void main(String[] args) {
        RandomizedQueue<Integer> q = new RandomizedQueue<Integer>();
        q.enqueue(0);
        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        q.enqueue(4);
        q.enqueue(5);
        q.enqueue(6);
        q.enqueue(7);
        q.enqueue(8);
        q.enqueue(9);
        q.dequeue();
        q.dequeue();
        for (int i : q) {
            StdOut.println(i);
        }
        StdOut.println("-----");
        for (int i : q) {
            StdOut.println(i);
        }
    }
}
