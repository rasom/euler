(ns euler.core
  (:require [clojure.math.numeric-tower :as m]
            [clojure.math.combinatorics :as c]
            [clojure.string :as s]
            [rhizome.viz :as r])
  (:gen-class))


(comment (def days (range 1000))
         (def flowers [:G :C :V])
         (defn trans [[l c r]] [r l c])
         (def sq (take 3 (iterate trans flowers)))
         (time (println (map #(nth sq (mod % 3)) days)))
         (time (println (map #(nth sq (mod % 3)) [5]))))

(comment
  (def nm (str "73167176531330624919225119674426574742355349194934"
               "96983520312774506326239578318016984801869478851843"
               "85861560789112949495459501737958331952853208805511"
               "12540698747158523863050715693290963295227443043557"
               "66896648950445244523161731856403098711121722383113"
               "62229893423380308135336276614282806444486645238749"
               "30358907296290491560440772390713810515859307960866"
               "70172427121883998797908792274921901699720888093776"
               "65727333001053367881220235421809751254540594752243"
               "52584907711670556013604839586446706324415722155397"
               "53697817977846174064955149290862569321978468622482"
               "83972241375657056057490261407972968652414535100474"
               "82166370484403199890008895243450658541227588666881"
               "16427171479924442928230863465674813919123162824586"
               "17866458359124566529476545682848912883142607690042"
               "24219022671055626321111109370544217506941658960408"
               "07198403850962455444362981230987879927244284909188"
               "84580156166097919133875499200524063689912560717606"
               "05886116467109405077541002256983155200055935729725"
               "71636269561882670428252483600823257530420752963450"))


  (def to-int #(Integer/parseInt (str %)))
  (time (println (apply max (map #(apply * %) (partition 13 1 (map to-int nm)))))))

(comment
  ;By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
  ;What is the 10 001st prime number?
  (defn prime? [num]
    (let [max-div (int (Math/sqrt num))]
      (not-any? zero? (map #(mod num %) (range 2 (inc max-div))))))
  (nth (filter prime? (iterate inc 1)) 10001))
(comment
  ;eul1
  (apply + (filter #(or (zero? (mod % 3)) (zero? (mod % 5))) (range 0 1000)))
  (defn add-next-fib [sq]
    (let [l (last sq)
          prel (nth sq (- (count sq) 2))]
      (conj sq (+ l prel))))

  (def sq (iterate add-next-fib [1 2]))
  (nth sq (count (take-while #(> 1000 %) sq)))
  (defn palindrom? [num]
    (let [s (str num)
          hf (int (/ (count s) 2))]
      (= (take hf s) (take hf (reverse s)))))
  (defn combs [sum]
    (mapcat
      (fn [a]
        (let [r (- sum a)]
          (map
            (fn [b] [a b (- r b)])
            (range a (-> r (/ 2) int inc)))))
      (range 1 (-> sum (/ 2) int inc)))))

(comment
  (defn divisible? [nm sq]
    (every? identity (map #(zero? (mod nm %)) sq)))

  (println (take 1 (filter d20? (iterate inc (apply * (filter prime? (range 1 21)))))))
  (def fib-seq
    ((fn rfib [a b]
       (lazy-seq (cons a (rfib b (+ a b)))))
      0 1))
  )

(comment
  ;eu457
  (defn f [n] (- (* n n) (* 3 n) 1))
  (def prm (filter prime? (range 1 10000000)))
  (last prm))
;9999991

(def to-int #(-> (str %) (s/replace #"^0" "") read-string))
(defn implode-to-digits [s] (to-int (apply str s)))
(defn explode-to-digits [number]
  (sort (map (comp read-string str) (seq (str number)))))

(first (sort-by count > (group-by #(nth % 1)
                                  (map #(let [c (m/expt % 3)] [% (explode-to-digits c)]) (range 1 1000)))))
;"Elapsed time: 23.109431 msecs"
;=> 5777
;[2,3,1,1,4]

;https://leetcode.com/problems/jump-game/
#_(defn check [s]
  (let [[n s] (split-at 1 s)]
    (filter #(<= ) (map-indexed (fn [i e] [(inc i) e (drop (inc i) s)])))))

(comment
  (defprotocol UF
    (connected? [_ p q])
    (union [t p q])
    (root [t i])
    (print-state [t]))

  (defrecord QF [v]
    UF
    (connected? [_ p q] (= (v p) (v q)))
    (union [t p q] (if (connected? t p q) t (QF. (replace {p q} v)))))


  (defrecord WQU [v sz]
    UF
    (print-state [t] (println t) t)
    (root [_ i] (loop [id i]
                  (if (= id (v id))
                    id
                    (recur (v id)))))
    (connected? [t p q] (= (root t p) (root t q)))
    (union [t p q]
      (println p "-" q)
      (if-not (connected? t p q)
        (let [rp (root t p)
              rq (root t q)
              szp (sz rp)
              szq (sz rq)
              total-sz (+ szq szp)]
          (apply ->WQU (if (> szp szq)
                         [(assoc v rq rp) (assoc sz rp total-sz)]
                         [(assoc v rp rq) (assoc sz rq total-sz)])))
        t)))

  (def data (vec (range 10)))
  (def sz (vec (repeat 10 1))))

;1-4 9-8 5-6 1-3 5-8 7-4 2-7 2-9 8-0
#_(-> (WQU. data sz)
  (union 1 4)
  (union 9 8)
  (union 5 6)
  (union 1 3)
  (union 5 8)
  (union 7 4)
  (union 2 7)
  (union 2 9)
  (union 8 0))
;#euler.core.WQU{:v [4 4 4 4 4 6 8 4 4 8], :sz [1 1 1 1 10 1 2 1 4 1]}
#_(-> (QF. data)
  (union 6 3)
  (union 8 7)
  (union 1 7)
  (union 5 7)
  (union 7 9)
  (union 2 0)
  :v)
;0 9 0 3 4 9 3 9 9 9
(defn get-roots [v]
  (vec (keep-indexed #(when (= %1 %2) %2) v)))
(defn get-childs [v i]
  (vec (keep-indexed #(when (and (= i %2) (not= i %1)) %1) v)))

#_(defn build-tree [v i]
  (map #(vector i (build-tree v %)) (get-childs v i)))

(defn build-lvl [v i]
  (into {} (merge
             (map #(let [ch (build-lvl v %)]
                    [% ch])
                  (get-childs v i)))))

(defn build-tree [v]
  (let [rs (get-roots v)]
    (into {} (map #(let [ch (build-lvl v %)] [% ch]) rs))))

(defn get-graph [v]
  (vec (filter #(= (first %) (v (first %))) (map #(vector % (get-childs v %)) (range (count v))))))

(defn draw [v]
  (r/view-tree (fn [_] true)
               #(cond (vector? %) (second %)
                      (map? %) (mapcat seq (vals %))
                      :default nil)
               (first (seq {:root v}))
               :node->descriptor
               (fn [n] {:label (first n)})
               ))

(defn ppp [s]
  (print (str (peek s) " "))
  (pop s))

(defn btl [s]
  (print (str (last s) " "))
  (butlast s))

(-> '()
    (conj 1)
    (conj 2)
    (conj 3)
    (conj 4)
    (conj 5)
    (conj 6)
    (conj 7)
    (conj 8)
    (conj 9)
    )