/**
 * Created by volosovskijroman on 04.07.15.
 */
public class St {
    public static void main(String[] args) {
        int n = 50;
        while(n > 0){
            StdOut.println(n % 2);
            n = n / 2;
        }
    }
}
