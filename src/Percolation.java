import java.util.SortedSet;
import java.util.TreeSet;

public class Percolation {
    private int[][] grid;
    private WeightedQuickUnionUF uf;
    private int enter;
    private SortedSet<Integer> last;
    private boolean percolates = false;

    public Percolation(int N) {
        if (N < 1) {
            throw new IllegalArgumentException();
        }
        int l = N * N + 1;
        enter = l - 1;
        uf = new WeightedQuickUnionUF(l);
        grid = new int[N][N];
        last = new TreeSet<Integer>();
    }

    public void open(int i, int j) {
        grid[i - 1][j - 1] = 1;
        int idx = getIndex(i, j);

        for (int k = -1; k < 2; k += 2) {
            for (int l = 0; l < 2; l++) {
                int ni = i;
                int nj = j;
                if (l == 1) {
                    ni = i + k;
                } else {
                    nj = j + k;
                }
                if (nj <= grid.length && nj > 0) {
                    if (ni > grid.length) {
                        checkLast(idx);
                    } else if (ni == 0) {
                        uf.union(idx, enter);
                    } else {
                        if (isOpen(ni, nj)) {
                            uf.union(idx, getIndex(ni, nj));
                        }
                    }
                }
            }
        }
    }

    private void checkLast(int idx) {
        boolean con = false;
        for (int idx2 : last) {
            if (uf.connected(idx, idx2)) {
                con = true;
                break;
            }
        }

        if (!con) {
            last.add(idx);
        }
    }

    public boolean isOpen(int i, int j) {
        return 1 == grid[i - 1][j - 1];
    }

    public boolean isFull(int i, int j) {
        return uf.connected(getIndex(i, j), enter);
    }

    public boolean percolates() {
        if (percolates) {
            return true;
        }
        for (int i : last) {
            if (uf.connected(enter, i)) {
                percolates = true;
                return true;
            }
        }

        return false;
    }

    private int getIndex(int i, int j) {
        if (i < 1 || i > grid.length
                || j < 1 || j > grid.length) {
            throw new IndexOutOfBoundsException();
        }
        return (i - 1) * grid.length + j - 1;
    }

    public static void main(String[] args) {
        int N = StdIn.readInt();
        Percolation p = new Percolation(N);

        int[] elms = StdIn.readAllInts();
        int idx = 0;
        while (idx < elms.length) {
            p.open(elms[idx], elms[idx + 1]);
            idx += 2;
        }

        for (int[] row : p.grid) {
            for (int el : row) {
                System.out.print(el + " ");
            }
            System.out.println();
        }

        System.out.println(p.isFull(2, 1));
        System.out.println(p.isOpen(2, 1));
        System.out.println(p.percolates());
    }
}
