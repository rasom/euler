public class PercolationStats {
    private double[] xs;
    private double mean = 0;
    private double stddev = 0;

    public PercolationStats(int N, int T) {
        if (N < 1 || T < 1) {
            throw new IllegalArgumentException();
        }
        xs = new double[T];

        for (int t = 0; t < T; t++) {
            Percolation p = new Percolation(N);
            int opened = 0;

            while (!p.percolates()) {
                int i = StdRandom.uniform(1, N + 1);
                int j = StdRandom.uniform(1, N + 1);
                if (!p.isOpen(i, j)) {
                    p.open(i, j);
                    opened++;
                }
            }

            xs[t] = (1.0 * opened) / (N * N);
        }

    }

    public double mean() {
        if (mean == 0) {
            mean = StdStats.mean(xs);
        }

        return mean;
    }

    public double stddev() {
        if (stddev == 0) {
            stddev = StdStats.stddev(xs);
        }

        return stddev;
    }

    public double confidenceLo() {
        return mean() - (1.96 * stddev() / (Math.sqrt(xs.length)));
    }

    public double confidenceHi() {
        return mean() + (1.96 * stddev() / (Math.sqrt(xs.length)));
    }

    public static void main(String[] args) {
        if (args.length != 2) {
            throw new IllegalArgumentException();
        }

        int N = Integer.parseInt(args[0]);
        int T = Integer.parseInt(args[1]);
        PercolationStats p = new PercolationStats(N, T);
        StdOut.printf("%-24s= %1.15f\n", "mean", p.mean());
        StdOut.printf("%-24s= %1.15f\n", "stddev", p.stddev());
        StdOut.printf("%-24s= %1.15f, %1.15f\n", "95% confidence interval", p.confidenceLo(), p.confidenceHi());
    }
}
