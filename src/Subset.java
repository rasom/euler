/**
 * Created by volosovskijroman on 26.07.15.
 */
public class Subset {
    public static void main(String[] args) {
        int k = Integer.parseInt(args[0]);
        String[] strings = StdIn.readAllStrings();
        StdRandom.shuffle(strings, 0, strings.length - 1);

        Deque<String> d = new Deque<String>();
        for (int i = 0; i < k; i++) {
            d.addFirst(strings[i]);
            StdOut.println(strings[i]);
        }
    }
}
